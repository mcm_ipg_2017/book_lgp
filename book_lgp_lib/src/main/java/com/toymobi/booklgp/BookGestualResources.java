package com.toymobi.booklgp;

interface BookGestualResources {

    int[] BOOK_PAGES = {
            R.raw.lgp_page_0,
            R.raw.lgp_page_1,
            R.raw.lgp_page_2,
            R.raw.lgp_page_3,
            R.raw.lgp_page_4,
            R.raw.lgp_page_5,
            R.raw.lgp_page_6,
            R.raw.lgp_page_7};

    int BOOK_PAGES_SIZE = BOOK_PAGES.length;

    int[] BOOK_PAGES_IMAGE = {
            R.drawable.capa,
            R.drawable.page_1,
            R.drawable.page_2,
            R.drawable.page_3,
            R.drawable.page_4,
            R.drawable.page_5,
            R.drawable.page_6,
            R.drawable.page_7,
            R.drawable.page_8};

    int[] BOOK_PAGES_LAYOUT = {R.layout.book_card_lgp,
            R.layout.book_card_lgp, R.layout.book_card_lgp,
            R.layout.book_card_lgp, R.layout.book_card_lgp,
            R.layout.book_card_lgp, R.layout.book_card_lgp,
            R.layout.book_card_lgp};

    int LAYOUTS_LENGTH = BOOK_PAGES_LAYOUT.length;

}
