package com.toymobi.booklgp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.toymobi.recursos.BaseFragment;

public class BookLGPFrag extends BaseFragment {

    private BookLGPController bookController;

    public static final String FRAGMENT_TAG_BOOK_LGP = "FRAGMENT_TAG_BOOK_LGP";

    @Override
    public final void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        createVibrationeedback();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (bookController != null) {
            bookController.playVideo();
        }

        //TODO ver isso depois parece mal
        if (bookController != null) {
            bookController.loadPage();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (bookController != null) {
            bookController.pauseVideo();
            bookController.savePage();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        deallocate();
    }

    @Override
    public final void onCreateOptionsMenu(@NonNull final Menu menu,
                                          @NonNull final MenuInflater inflater) {

        menu.clear();

        inflater.inflate(R.menu.menu_book_lgp, menu);

        // Set an icon in the ActionBar
        final MenuItem itemPlay = menu.findItem(R.id.play_menu_lgp);

        if (itemPlay != null) {
            bookController.menuItemPlay = itemPlay;

            if (BookLGPController.isPlayingVideo) {
                itemPlay.setIcon(R.drawable.icon_pause);
                itemPlay.setTitle(R.string.pause_menu);
            } else {
                itemPlay.setIcon(R.drawable.icon_play);
                itemPlay.setTitle(R.string.play_menu);
            }
        }

        final MenuItem itemPageNumber = menu.findItem(R.id.number_page_menu_book_lgp);

        if (itemPageNumber != null) {
            bookController.menuItemPageNumber = itemPageNumber;
            bookController.changePageNumberText();
        }

    }

    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   final ViewGroup container,
                                   final Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_booklgp, container, false);

        createToolBar(R.id.toolbar_booklgp, R.string.book_lgp_name);

        if (bookController == null) {
            bookController = new BookLGPController(getActivity(), inflater);
        }

        bookController.createPlayer(mainView);

        bookController.startBookText(mainView);

        return mainView;
    }

    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item) {

        final int item_id = item.getItemId();

        playVibrationFeedback();

        if (item_id == R.id.back_menu_book_lgp) {

            exitOrBackMenu(startSingleApp);

        } else if (item_id == R.id.play_menu_lgp) {
            BookLGPController.isPlayingVideo ^= true;

            if (BookLGPController.isPlayingVideo) {
                item.setIcon(R.drawable.icon_pause);
                item.setTitle(R.string.pause_menu);
                bookController.playVideo();
            } else {
                item.setIcon(R.drawable.icon_play);
                item.setTitle(R.string.play_menu);
                bookController.pauseVideo();
            }
        } else if (item_id == R.id.preview_page_menu_book_lgp) {

            if (bookController.pageNumber != 0) {
                bookController.bookPagePreviews();
            }

        } else if (item_id == R.id.next_page_menu_book_lgp) {

            if (bookController.pageNumber != BookGestualResources.LAYOUTS_LENGTH - 1) {
                bookController.bookPageNext();
            }

        } else {
            super.onOptionsItemSelected(item);
        }
        return true;
    }


    @Override
    public void deallocate() {

        super.deallocate();

        if (bookController != null) {
            bookController.clearAll();
            bookController = null;
        }

        if (mainView != null) {
            ((ConstraintLayout) mainView).removeAllViews();
        }
    }
}
