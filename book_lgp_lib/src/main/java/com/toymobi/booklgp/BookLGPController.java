package com.toymobi.booklgp;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.collection.SparseArrayCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.viewpager.widget.ViewPager;

import com.toymobi.framework.adapter.ReducedCustomPagerAdapter;
import com.toymobi.framework.debug.DebugUtility;
import com.toymobi.framework.media.VideoMediaControl;
import com.toymobi.framework.persistence.PersistenceManager;
import com.toymobi.recursos.EduardoStuff;

class BookLGPController {

    static boolean isPlayingVideo = false;

    private ViewPager viewPager;

    private ReducedCustomPagerAdapter adapter;

    int pageNumber;

    private CharSequence pageNumberText;

    private Context context;

    private SparseArrayCompat<Drawable> pageImages = null;

    MenuItem menuItemPageNumber;

    MenuItem menuItemPlay;

    private static final String PERSISTENCE_KEY_PAGE = "PERSISTENCE_KEY_PAGE";

    private VideoMediaControl videoMediaControl;

    private VideoView videoView;

    private MediaController mediaController;

    private static int currentPosition;

    private SparseArrayCompat<String> videosPathString = null;

    BookLGPController(final Context context, final LayoutInflater layoutInflater) {
        DebugUtility.printDebug("BookController Construtor");
        if (context != null) {

            this.context = context;

            start(layoutInflater);
        }
    }

    private void createAdapter(final LayoutInflater layoutInflater) {

        if (context != null && adapter == null && layoutInflater != null) {

            final int size = BookGestualResources.LAYOUTS_LENGTH;

            adapter = new ReducedCustomPagerAdapter(size);

            for (int i = 0; i < size; i++) {

                View temp = layoutInflater.inflate(BookGestualResources.BOOK_PAGES_LAYOUT[i], null);

                adapter.addViewPage(i, temp);
            }
        }
    }

    private void createViewPager() {
        if (adapter != null) {
            viewPager.setAdapter(adapter);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(final int page) {
                    stopVideo();

                    pageNumber = page;

                    pageNumberText = "" + (pageNumber + 1);

                    if (menuItemPageNumber != null) {
                        menuItemPageNumber.setTitle(pageNumberText);
                    }
                    play();
                }

                @Override
                public void onPageScrolled(final int page,
                                           final float positionOffset,
                                           final int positionOffsetPixels) {
                }

                @Override
                public void onPageScrollStateChanged(final int position) {
                }
            });
        }
    }

    final void startBookText(final View view) {
        if (view != null) {
            viewPager = view.findViewById(R.id.viewPagerLgp);
        }
        createViewPager();
    }

    private void goBookPage(final int page) {
        stopVideo();
        if (viewPager != null
                && pageNumber < BookGestualResources.LAYOUTS_LENGTH
                && page < BookGestualResources.LAYOUTS_LENGTH) {
            pageNumber = page;
            viewPager.setCurrentItem(pageNumber);
            play();
        }
    }

    final void bookPagePreviews() {
        if (viewPager != null) {
            if (pageNumber > 0) {
                stopVideo();
                --pageNumber;
                viewPager.setCurrentItem(pageNumber);
                play();
            } else {
                pageNumber = BookGestualResources.BOOK_PAGES_SIZE - 1;
                goBookPage(pageNumber);
            }
        }
    }

    final void bookPageNext() {
        if (viewPager != null) {
            if (pageNumber < BookGestualResources.LAYOUTS_LENGTH - 1) {
                stopVideo();
                ++pageNumber;
                viewPager.setCurrentItem(pageNumber);
                play();
            } else {
                pageNumber = 0;
                goBookPage(pageNumber);
            }
        }
    }

    private void createPageImage() {

        if (context != null) {

            final int size = BookGestualResources.BOOK_PAGES_IMAGE.length;

            if (pageImages == null) {

                pageImages = new SparseArrayCompat<>(size);

                final Resources resources = context.getResources();

                if (resources != null) {
                    for (int i = 0; i < size; i++) {

                        final int pageIndexID = BookGestualResources.BOOK_PAGES_IMAGE[i];

                        if (pageIndexID != -1) {

//                            final Drawable imageDrawable = resources.getDrawable(pageIndexID);
                            final Drawable imageDrawable = ResourcesCompat.getDrawable(resources, pageIndexID, null);

                            if (imageDrawable != null) {
                                pageImages.put(i, imageDrawable);
                            } else {
                                pageImages.put(i, null);
                            }

                        } else {
                            pageImages.put(i, null);
                        }
                    }
                }
            }
        }
    }

//    private void createPageImageLow() {
//
//        if (context != null) {
//
//            final int size = BookGestualResources.BOOK_PAGES_IMAGE.length;
//
//            final int height = DeviceDimensionsUtil.getDisplayHeight(context) / 2;
//
//            final int width = height + (height / 4);
//
//            if (pageImages == null) {
//
//                pageImages = new SparseArrayCompat<>(size);
//
//                final Resources resources = context.getResources();
//
//                if (resources != null) {
//
//                    BitmapDrawable bkg_image_temp;
//
//                    for (int i = 0; i < size; i++) {
//
//                        final int pageIndexID = BookGestualResources.BOOK_PAGES_IMAGE[i];
//
//                        if (pageIndexID != -1) {
//
//                            bkg_image_temp = new BitmapDrawable(resources,
//                                    UIComponentUtils
//                                            .decodeSampledBitmapFromResource(
//                                                    resources, pageIndexID,
//                                                    width, height));
//
//                            pageImages.put(i, bkg_image_temp);
//
//                        } else {
//                            pageImages.put(i, null);
//                        }
//                    }
//                }
//            }
//        }
//    }

    private void loadPageImage() {

        if (adapter != null && pageImages != null && pageImages.size() > 0) {

            for (int index = 0; index < BookGestualResources.BOOK_PAGES_LAYOUT.length; index++) {

                final View viewTemp = adapter.getViewPage(index);

                if (viewTemp != null) {

                    final ImageView image = viewTemp.findViewById(R.id.book_page_layout_image);

                    if (image != null) {

                        final int pageImageSize = pageImages.size();

                        if (pageImageSize > index) {
                            final Drawable imageDrawable = pageImages
                                    .get(index);

                            if (imageDrawable != null) {
                                image.setImageDrawable(imageDrawable);
                            }
                        }
                    }
                }
            }
        }
    }

    private void start(@NonNull final LayoutInflater layoutInflater) {

        createAdapter(layoutInflater);

        createPageImage();

        /*if (EduardoStuff.isNormalSize(context)) {
            createPageImage();
        } else {
            createPageImageLow();
        }*/

        createVideosPath();

        loadPageImage();

    }

    final void changePageNumberText() {
        if (menuItemPageNumber != null) {
            if (pageNumber == BookGestualResources.BOOK_PAGES_LAYOUT.length) {
                pageNumberText = ""
                        + BookGestualResources.BOOK_PAGES_LAYOUT.length;
            } else {
                pageNumberText = "" + (pageNumber + 1);
            }

            menuItemPageNumber.setTitle(pageNumberText);
        }
    }

    final void clearAll() {
        stopVideo();

        if (adapter != null) {
            for (int index = 0; index < BookGestualResources.BOOK_PAGES_LAYOUT.length; index++) {

                View viewTemp = adapter.getViewPage(index);

                if (viewTemp != null) {

                    ImageView picture = viewTemp.findViewById(R.id.book_page_layout_image);

                    if (picture != null) {
                        picture.setImageDrawable(null);
                        picture.setImageBitmap(null);
                        //picture = null;
                    }
                    //viewTemp.destroyDrawingCache();
                    //viewTemp = null;
                }
            }
            adapter.deallocate();
            adapter = null;
        }

        if (viewPager != null) {
            //viewPager.destroyDrawingCache();
            viewPager.removeAllViews();
            viewPager.removeAllViewsInLayout();
            viewPager = null;
        }

        if (pageImages != null) {

            /*final int sizeImages = pageImages.size();

            for (int i = 0; i < sizeImages; i++) {
                Drawable imageDrawable = pageImages.get(i);

                if (imageDrawable != null) {
                    imageDrawable = null;
                }
            }*/
            pageImages.clear();
            pageImages = null;
        }
        if (videosPathString != null) {
            videosPathString.clear();
            videosPathString = null;
        }

        videoView = null;

        videoMediaControl = null;

        menuItemPageNumber = null;

        menuItemPlay = null;
    }

    final void savePage() {
        if (EduardoStuff.ENABLE_SAVE_PAGE_BOOK) {
            PersistenceManager.saveSharedPreferencesInt(context,
                    PERSISTENCE_KEY_PAGE, pageNumber);
        }
    }

    final void loadPage() {

        if (EduardoStuff.ENABLE_SAVE_PAGE_BOOK) {
            pageNumber = PersistenceManager.loadSharedPreferencesInt(context,
                    PERSISTENCE_KEY_PAGE, 0);
        }
        goBookPage(pageNumber);
    }

    final void createPlayer(final View rootView) {
        if (rootView != null) {
            if (videoMediaControl == null) {
                videoMediaControl = new VideoMediaControl();

                videoView = rootView.findViewById(R.id.videoView);

                if (videoView != null && context != null) {

                    mediaController = new MediaController(context) {
                        @Override
                        public void hide() {
                            mediaController.show();
                        }

                        @Override
                        public void setAnchorView(final View view) {

                        }
                    };

                    mediaController.setMediaPlayer(videoMediaControl);

                    videoView.setMediaController(mediaController);

                    videoView.requestFocus();
                }
                play();
            }
        }
    }

    private void play() {

        if (context != null && videoView != null && videosPathString != null
                && videosPathString.size() > 0) {

            videoView.setVideoURI(Uri.parse(videosPathString.get(pageNumber)));

            videoView.start();

            if (menuItemPlay != null) {
                menuItemPlay.setIcon(R.drawable.icon_pause);
                menuItemPlay.setTitle(R.string.pause_menu);
                isPlayingVideo = true;
            }

            videoView.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(final MediaPlayer mediaPlayer) {
                    if (mediaPlayer != null) {
                        currentPosition = mediaPlayer.getDuration();

                        if (menuItemPlay != null) {
                            menuItemPlay.setIcon(R.drawable.icon_play);
                            menuItemPlay.setTitle(R.string.play_menu);
                            isPlayingVideo = false;
                        }
                    }
                }
            });
            mediaController.show(0);
        }
    }

    private void createVideosPath() {
        if (videosPathString == null && context != null) {

            videosPathString = new SparseArrayCompat<>(
                    BookGestualResources.BOOK_PAGES_SIZE);

            final String path = "android.resource://"
                    + context.getPackageName() + "/";

            int i = 0;

            for (final int id : BookGestualResources.BOOK_PAGES) {
                videosPathString.put(i, path + id);
                i++;
            }
        }
    }

    final void playVideo() {
        if (videoView != null) {
            final int duration = videoMediaControl.getDuration();

            if (currentPosition < duration) {
                videoView.seekTo(currentPosition);
            } else {
                videoView.seekTo(0);
            }

            if (menuItemPlay != null) {
                menuItemPlay.setIcon(R.drawable.icon_pause);
                menuItemPlay.setTitle(R.string.pause_menu);
                isPlayingVideo = true;
            }
            videoView.start();
        }
    }

    void pauseVideo() {
        if (videoView != null && menuItemPlay != null) {
            currentPosition = videoMediaControl.getCurrentPosition();
            videoView.pause();
            menuItemPlay.setIcon(R.drawable.icon_play);
            menuItemPlay.setTitle(R.string.play_menu);
            isPlayingVideo = false;
        }
    }

    private void stopVideo() {
        if (videoView != null) {
            videoView.stopPlayback();
        }
    }
}
