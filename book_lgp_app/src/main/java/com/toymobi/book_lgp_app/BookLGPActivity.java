package com.toymobi.book_lgp_app;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.toymobi.booklgp.BookLGPFrag;
import com.toymobi.recursos.BaseFragment;

public class BookLGPActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_book_lgp);
        startBookFrag();
    }

    private void startBookFrag() {

        BookLGPFrag.startSingleApp = true;

        final FragmentManager fragmentManager = getSupportFragmentManager();

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        final BaseFragment fragmentBook = new BookLGPFrag();

        fragmentTransaction.replace(R.id.main_layout_fragment_id, fragmentBook, BookLGPFrag.FRAGMENT_TAG_BOOK_LGP);

        fragmentTransaction.commit();
    }
}
